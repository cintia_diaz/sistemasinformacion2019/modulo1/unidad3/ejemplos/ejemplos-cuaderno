DROP DATABASE IF EXISTS ejemplo2programacion;
CREATE DATABASE ejemplo2programacion;
USE ejemplo2programacion;

-- Creaci�n de la tabla datos para los procedimientos 4, 5, 6, 7 y 8

CREATE TABLE IF NOT EXISTS datos(
  datos_id int NOT NULL AUTO_INCREMENT,
  numero1 int NOT NULL,
  numero2 int NOT NULL,
  suma varchar(255) DEFAULT NULL,
  resta varchar(255) DEFAULT NULL,
  rango varchar(5) DEFAULT NULL,
  texto1 varchar(25) DEFAULT NULL,
  texto2 varchar(25) DEFAULT NULL,
  junto varchar(255) DEFAULT NULL,
  longitud varchar(255) DEFAULT NULL,
  tipo int NOT NULL,
  numero int NOT NULL,
  PRIMARY KEY (datos_id)
  );

ALTER TABLE datos
  ADD INDEX numero2_index (numero2);

ALTER TABLE datos
  ADD INDEX numero_index (numero);

ALTER TABLE datos
  ADD INDEX tipo_index (tipo);

INSERT INTO datos (numero1, numero2, rango, texto1, texto2)
  VALUES  (2, 45, 'A', 'Mi casa', 'tiene persianas'),  
          (34, 67, 'A', 'El cielo', 'es azul'),
          (78, 65, 'B', 'El agua del mar', 'es salada'),
          (67, 89, 'C', 'Nosotros', 'comemos pasta'),
          (89, 67, 'A', 'Ellos', 'trabajan bastante'),
          (67, 56, 'B', 'La pizarra', 'est� apagada'),
          (89, 67, 'B', 'El ordenador', NULL),
          (76, 67, 'z', 'La mochila', 'no tiene asas'),
          (99, 99, 'A', 'El agua', 'est� fria'),
          (99, 99, 'C', 'El rat�n', 'va muy r�pido');



/*
  PROCEDIMIENTO 1
  Realizar un procedimiento almacenado que reciba un texto y un car�cter. Debe indicarte si ese car�cter est� en el 
  texto. Deb�is realizarlo con: Locate, Position
*/
  -- version 1a con la funci�n LOCATE
  DELIMITER $$
    CREATE OR REPLACE PROCEDURE ej1a(texto varchar(255), caracter varchar(1))
      BEGIN
        IF LOCATE(caracter, texto)>0 THEN
          SELECT CONCAT('El caracter ',caracter, ' est� en el texto' );
        ELSE
          SELECT CONCAT('El caracter ', caracter, ' no est� en el texto');
        END IF;        
      END $$
    DELIMITER ;

  CALL ej1a('Escribimos $ en la frase','%');


  -- version 1b con la funci�n POSITION
    DELIMITER $$
      CREATE OR REPLACE PROCEDURE ej1b(texto varchar(255), caracter varchar(1))
        BEGIN
          IF POSITION(caracter IN texto)>0 THEN
            SELECT CONCAT('El caracter ',caracter, ' est� en el texto' );
          ELSE
            SELECT CONCAT('El caracter ', caracter, ' no est� en el texto');
          END IF;        
        END $$
      DELIMITER ;

  
  CALL ej1b('Escribimos �? en la frase','?');

/*
  PROCEDIMIENTO 2
  Realizar un procedimiento almacenado que reciba un texto y un car�cter. Te debe indicar todo el texto que haya
  antes de la primera vez que aparece ese car�cter
*/

  DELIMITER $$
  CREATE OR REPLACE PROCEDURE ej2(texto varchar(50), caracter char(1))
    BEGIN
    
      SELECT SUBSTRING_INDEX(texto,caracter,1);
      
    
    END $$
  DELIMITER ;

CALL ej2('hola, �qu� tal?','�');
  

/*
  PROCEDIMIENTO 3
  Realizar un procedimiento almacenado que reciba tres n�meros y dos argumentos de tipo salida donde devuelva el 
  n�mero m�s grande y el n�mero m�s peque�o de los tres n�meros pasados
*/

  -- version 3a con las funciones de MySQL greates y least
  DELIMITER $$
  CREATE OR REPLACE PROCEDURE ej3a(n1 int, n2 int, n3 int, OUT grande int, OUT peque�o int)
    BEGIN
      SET grande=GREATEST(n1, n2, n3);
      SET peque�o=LEAST(n1,n2,n3);
      
    END $$
  DELIMITER ;
  

CALL ej3(3, 4, -2,@g, @p);
SELECT @g, @p;

-- version 3b con if
  DELIMITER $$
  CREATE OR REPLACE PROCEDURE ej3b(n1 int, n2 int, n3 int)
    BEGIN
      DECLARE grande int DEFAULT 0;
      DECLARE peque�o int DEFAULT 0;

      IF (n1>n2) THEN
       IF (n1>n3) THEN
         SET grande=n1;
       ELSE 
         SET grande=n3;
       END IF;
      ELSE
        IF (n2>n3) THEN
          SET grande=n2;
        ELSE
          SET grande=n3;
        END IF;
      END IF;

    IF (n1<n2) THEN
      IF (n1<n3) THEN
        SET peque�o=n1;
      ELSE
        SET peque�o=n3;
      END IF;   	
    ELSE
      IF (n2<n3) THEN
        SET peque�o=n2;      
      ELSE
        SET peque�o=n3;     
      END IF;         
    END IF;

    SELECT grande, peque�o; 
      
    END $$
  DELIMITER ;
  

CALL ej3b(4,2,-3);



/*
  PROCEDIMIENTO 4
  Realizar un procedimiento almacenado que muestre cuantos numeros1 y numeros2 son mayores que 50
*/

  DELIMITER $$
  CREATE OR REPLACE PROCEDURE ej4(OUT n1 int, OUT n2 int)
    BEGIN
    -- cuenta cuantos numeros1 son mayores que 50
      SET n1=(SELECT COUNT(*) FROM datos WHERE numero1>50);
    -- cuenta cuantos numeros2 son mayores que 50
      SET n2=(SELECT COUNT(*) FROM datos WHERE numero2>50);
    END $$
  DELIMITER ;
  
  CALL ej4(@n1s,@n2s);
  SELECT @n1s,@n2s;

/*
  PROCEDIMIENTO 5
  Realizar un procedimiento almacenado que calcule la suma y la resta de numero1 y numero2
*/
 
 DELIMITER $$
  CREATE OR REPLACE PROCEDURE ej5()
    BEGIN
    -- actualiza la tabla modificando el campo suma y el campo resta
      UPDATE datos SET suma=numero1+numero2;
      UPDATE datos SET resta=numero1-numero2;
    END $$
 DELIMITER ;

CALL ej5();
SELECT * FROM datos;
  
/*
  PROCEDIMIENTO 6
  Realizar un procedimiento almacenado que primero ponga todos los valores de suma y resta a NULL y despues
  calcule la suma solamente si el numero1 es mayor que el numero2 y calcule la resta de numero2-numero1 si el numero2
  es mayor o numero1-numero2 si es mayor el numero1
*/

  DELIMITER $$
  CREATE OR REPLACE PROCEDURE ej6()
    BEGIN

    -- pone todos los valores de suma y resta a NULL
      UPDATE datos SET suma=NULL;
      UPDATE datos SET resta=NULL;

  /*    IF ((SELECT numero1 FROM datos)>(SELECT numero2 FROM datos)) THEN */

    -- modifica el campo suma y resta seg�n las condiciones
      UPDATE datos SET suma=numero1+numero2 WHERE numero1>numero2;
      UPDATE datos SET resta=numero1-numero2 WHERE numero1>numero2;      
      
      UPDATE datos SET resta=numero2-numero1 WHERE numero2>=numero1;      
     
      
    END $$
  DELIMITER ;
  

CALL ej6();
SELECT * FROM datos;


/*
  PROCEDIMIENTO 7
  Realizar un procedimiento almacenado que coloque en el campo junto
  el texto1, texto2
*/

  DELIMITER $$
  CREATE OR REPLACE PROCEDURE ej7()
    BEGIN
      UPDATE datos SET junto=CONCAT_WS(" ", texto1, texto2);
    END $$
  DELIMITER ;

CALL ej7();
SELECT * FROM datos;

/*
  PROCEDIMIENTO 8
  Realizar un procedimiento almacenado que coloque en el campo junto el valor NULL. Despu�s debe colocar en el
  campo junto el texto1-texto2 si el rango es A y si es rango B debe colocar texto1+texto2. Si el rango es distinto 
  debe colocar texto1 nada m�s
*/
  
  DELIMITER $$
  CREATE OR REPLACE PROCEDURE ej8()
    BEGIN

      -- inicializo poniendo el campo junto a NULL
      UPDATE datos SET junto=NULL;

      UPDATE datos SET junto=CONCAT_WS("-",texto1,texto2) WHERE rango='A';
      UPDATE datos SET junto=CONCAT_WS("+",texto1,texto2) WHERE rango='B';
      UPDATE datos SET junto=texto1 WHERE rango!='A' AND rango!='B';
      
    END $$
  DELIMITER ;
  

CALL ej8();
SELECT * FROM datos;