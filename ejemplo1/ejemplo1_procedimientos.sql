﻿DROP DATABASE IF EXISTS ejemplo1programacion;
CREATE DATABASE ejemplo1programacion;
USE ejemplo1programacion;

/*
  PROCEDIMIENTO 1
  Realizar un procedimiento almacenado que reciba dos números y te indique el mayor de ellos 
  (realizarle con instrucción if, con consulta de totales y con una función de MySQL)
*/
-- Con la instrucción if
DELIMITER $$
CREATE OR REPLACE PROCEDURE ej1v1(numero1 int, numero2 int)
  BEGIN
    IF numero1<numero2 THEN 
      SELECT numero2;
    ELSE 
      SELECT numero1;
    END IF;    
  END $$
DELIMITER ;


CALL ej1v1(123,23);

-- Con una tabla temporal y una consulta de totales
DELIMITER $$
CREATE OR REPLACE PROCEDURE ej1v2a(numero1 int, numero2 int)
  BEGIN
  
  -- esto es una tabla temporal
  CREATE OR REPLACE TABLE numeros(
    a int
    );

  /*
  si haces create table if not exists, luego tienes que hacer drop table despues del select max
  CREATE TABLE IF NOT EXISTS numeros(
    a int
    );
  */
    
  INSERT INTO numeros VALUES (numero1), (numero2);

  SELECT MAX(a) FROM numeros;
  
  /*
  DROP TABLE numeros;
  */
    
  END $$
DELIMITER ;

CALL ej1v2a(12,23);

SELECT * FROM numeros;

CALL ej1v2(45,85);

 -- version 2 mejorada
DELIMITER $$
CREATE OR REPLACE PROCEDURE ej1v2b(numero1 int, numero2 int)
  BEGIN
   CREATE TEMPORARY TABLE numerosb(
    a int
    );
  INSERT INTO numerosb VALUES (numero1), (numero2);
    
  SELECT MAX(a) FROM numerosb;
  END $$
DELIMITER ;

CALL ej1v2b(23, 56);



-- Con la función de mysql GREATEST
DELIMITER $$
CREATE OR REPLACE PROCEDURE ej1v3(numero1 int, numero2 int)
  BEGIN
    SELECT GREATEST(numero1, numero2);
  END $$
DELIMITER ;

CALL ej1v3(12,23);

/*
  PROCEDIMIENTO 2
  Realizar un procedimiento almacenado que reciba tres números y te indique
  el mayor de ellos (realizarle con instrucción if, con consulta de totales y
  con una función de MySQL)
*/

  -- v1: Con la instrucción if
  DELIMITER $$
  CREATE OR REPLACE PROCEDURE ej2v1(numero1 int, numero2 int, numero3 int)
    BEGIN
    IF numero1 > numero2 THEN
        IF numero1 > numero3 THEN
          SELECT numero1;
        ELSE
          SELECT numero3;
        END IF;
   ELSE
    IF numero2 > numero3 THEN
      SELECT numero2;
    ELSE
      SELECT numero3;
    END IF;
   END IF;          
    END $$
  DELIMITER ;
  
CALL ej2v1(86,87,85);


-- v2: Con una tabla temporal y una consulta de totales
DELIMITER $$
CREATE OR REPLACE PROCEDURE ej2v2(numero1 int, numero2 int, numero3 int)
  BEGIN
    CREATE OR REPLACE TEMPORARY TABLE numeros2(
    a int
    );
  INSERT INTO numeros2 VALUES (numero1), (numero2), (numero3);
    
  SELECT MAX(a) FROM numeros2;
  /*
  create temporary table if not exists
  drop table 
  */
  END $$
DELIMITER ;


CALL ej2v2(83,86,89);
SELECT * FROM numeros2;


-- v3: Con la función de mysql GREATEST
DELIMITER $$
CREATE OR REPLACE PROCEDURE ej2v3(numero1 int, numero2 int, numero3 int)
  BEGIN
    SELECT GREATEST(numero1, numero2, numero3);
  END $$
DELIMITER ;

CALL ej2v3(12,23,34);


/*
  PROCEDIMIENTO 3
  Realizar un procedimiento almacenado que reciba tres números y dos argumentos
  de tipo salida donde devuelva el número más grande y el número más pequeño de
  los tres números pasados
*/

  DELIMITER $$
  CREATE OR REPLACE PROCEDURE ej3(numero1 int, numero2 int, numero3 int, OUT g int, OUT p int)
    BEGIN
      SET g=GREATEST(numero1, numero2, numero3);
      SET p=LEAST(numero1, numero2, numero3);      
    END $$
  DELIMITER ;
  
CALL ej3(12,23,55,@grande,@pequenio);
SELECT @grande,@pequenio;


/*
  PROCEDIMIENTO 4
  Realizar un procedimiento almacenado que reciba dos fechas y te muestre el número
  de días de diferencia entre las dos fechas
*/

  DELIMITER $$
  CREATE OR REPLACE PROCEDURE ej4(fecha1 date, fecha2 date)
    BEGIN
    SELECT ABS(TIMESTAMPDIFF(DAY, fecha1, fecha2)) dias;      
    END $$
  DELIMITER ;

CALL ej4('2010/1/1','2010/1/10');

  

/*
  PROCEDIMIENTO 5
  Realizar un procedimiento almacenado que reciba dos fechas y te muestre el número
  de meses de diferencia entre las dos fechas
*/

   DELIMITER $$
    CREATE OR REPLACE PROCEDURE ej5(fecha1 date, fecha2 date)
      BEGIN
        SELECT ABS(TIMESTAMPDIFF(MONTH, fecha1, fecha2)) meses;        
      END $$
    DELIMITER ;

CALL ej5('2010/1/1','2010/1/10');



/*
  PROCEDIMIENTO 6
  Realizar un procedimiento almacenado que reciba dos fechas y te devuelva en 
  3 argumentos de salida los días, meses y años entre las dos fechas
*/

   DELIMITER $$
    CREATE OR REPLACE PROCEDURE ej6(fecha1 date, fecha2 date, OUT dias int, OUT meses int, OUT anios int)
      BEGIN
        SET dias=ABS(TIMESTAMPDIFF(DAY, fecha1, fecha2));
        SET meses=ABS(TIMESTAMPDIFF(MONTH, fecha1, fecha2));        
        SET anios=ABS(TIMESTAMPDIFF(YEAR, fecha1, fecha2));                
      END $$
    DELIMITER ;

CALL ej6('2010/1/1','2020/1/1', @d,@m,@a);
SELECT @d dias, @m meses, @a años;


/*
  PROCEDIMIENTO 7
  Realizar un procedimiento almacenado que reciba una frase y te muestre el 
  número de carácteres
*/

   DELIMITER $$
    CREATE OR REPLACE PROCEDURE ej7(frase varchar(255))
      BEGIN
        SELECT CHAR_LENGTH(frase) caracteres;
      END $$
    DELIMITER ;

CALL ej7('Hola, esto es procedimiento 7');