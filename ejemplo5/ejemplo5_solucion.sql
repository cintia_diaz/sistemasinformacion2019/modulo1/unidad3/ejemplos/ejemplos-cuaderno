﻿DROP DATABASE IF EXISTS ejemplo5programacion;
CREATE DATABASE ejemplo5programacion;
USE ejemplo5programacion;

/*
  PROCEDIMIENTO 1
  Escribe un procedimiento que reciba el nombre de un país como parámetro de entrada y
  realice una consulta sobre la tabla cliente para obtener todos los clientes que existen en
  la tabla de ese país.
*/
  DELIMITER $$
  CREATE OR REPLACE PROCEDURE procedimiento1(vpais varchar(50))
    BEGIN
      SELECT * FROM cliente WHERE pais=vpais;
    END $$
  DELIMITER ;
  
  CALL procedimiento1('usa');

/*
  PROCEDIMIENTO 2
  Realizar una función que me devuelva el número de registros de la tabla clientes que
  pertenezcan a un país que le pase por teclado.
*/
  DELIMITER &&
  CREATE OR REPLACE FUNCTION funcion2(vpais varchar(50))
    RETURNS int
  BEGIN
    DECLARE numero int DEFAULT 0;
      SELECT COUNT(*) INTO numero FROM cliente WHERE pais=vpais;
    RETURN numero;
      
  END &&
  DELIMITER ;

SELECT funcion2('usa');

/*
  PROCEDIMIENTO 3
  Realizar el mismo ejercicio anterior, pero para desplazarme por la tabla utilizar
  cursores con excepciones.
*/
/*
  PROCEDIMIENTO 4
  Escribe una función que le pasas una cantidad y que te devuelva el número total de
  productos que hay en la tabla productos cuya cantidad en stock es superior a la
  pasada.
*/
  DELIMITER &&
  CREATE OR REPLACE FUNCTION funcion4(n1 int)
    RETURNS int
  BEGIN
    DECLARE numTotal int DEFAULT 0;
      SELECT COUNT(*) INTO numTotal FROM producto WHERE cantidad_en_stock>n1;
    RETURN numTotal;
  END &&
  DELIMITER ;

  SELECT funcion4(20);

/*
  PROCEDIMIENTO 5
  Realizar el mismo ejercicio anterior pero con cursores
*/
/*
  PROCEDIMIENTO 6
  Escribe un procedimiento que reciba como parámetro de entrada una forma de pago,
  que será una cadena de caracteres (Ejemplo: PayPal, Transferencia, etc). Y devuelva
  como salida el pago de máximo valor realizado para esa forma de pago. Deberá hacer
  uso de la tabla pago de la base de datos jardinería.
*/
  DELIMITER &&
  CREATE OR REPLACE PROCEDURE procedimiento6(formaPago varchar(40))
    BEGIN
      SELECT MAX(total) FROM pago WHERE forma_pago=formaPago;
    END &&
  DELIMITER ;

  CALL procedimiento6('cheque');
  
/*
  PROCEDIMIENTO 7
  Escribe un procedimiento que reciba como parámetro de entrada una forma de pago,
  que será una cadena de caracteres (Ejemplo: PayPal, Transferencia, etc). Y devuelva
  como salida los siguientes valores teniendo en cuenta la forma de pago seleccionada
  como parámetro de entrada: 
    el pago de máximo valor, el pago de mínimo valor, el valor medio de los pagos realizados,
    la suma de de todos los pagos, el número de pagos realizados para esa forma de pago
*/

DELIMITER &&
CREATE OR REPLACE PROCEDURE procedimiento7(formaP varchar(40))
  BEGIN
    SELECT 
      MAX(total) maximoValor,
      MIN(total) minimoValor,
      AVG(total) valorMedio,
      SUM(total) sumaPago,
      COUNT(*) nPagos
    FROM pago 
    WHERE forma_pago=formaP;
  END &&
DELIMITER ;

CALL  procedimiento7('transferencia');


/*
  PROCEDIMIENTO 8
  Escribe una función para la tabla productos que devuelva el valor medio del precio de
  venta de los productos de un determinado proveedor que se recibirá como parámetro de
  entrada. El parámetro de entrada será el nombre del proveedor.
*/
  DELIMITER &&
  CREATE OR REPLACE FUNCTION funcion8(nomProve varchar(50))
    RETURNS float
  BEGIN
    DECLARE precioV float DEFAULT 0;

      SELECT AVG(precio_venta) INTO precioV FROM producto WHERE proveedor=nomProve;
   
    RETURN precioV ;
  END &&
  DELIMITER ;

  SELECT funcion8('Frutales Talavera S.A');
  
  /*
  PROCEDIMIENTO 9
  Realizar el mismo ejercicio anterior, pero haciendo que en caso de que el fabricante
  pasado no tenga productos produzca una excepción personalizada que muestre el error
  fabricante no existe.
*/