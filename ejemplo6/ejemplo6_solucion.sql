﻿-- DROP DATABASE IF EXISTS ejemplo6programacion;
-- CREATE DATABASE ejemplo6programacion;
USE ejemplo6programacion;

  /** DISPARADORES **/

/*
  EJEMPLO 1
  Crear un disparador para la tabla ventas para que cuando metas un registro nuevo te
  calcule el total automáticamente.
  corregido
*/
  
  DELIMITER $$
  CREATE OR REPLACE TRIGGER ventasBI
    BEFORE INSERT
      ON ventas
      FOR EACH ROW
    BEGIN
      SET NEW.total=NEW.precio*NEW.unidades;
    END $$
  DELIMITER ;

  INSERT INTO ventas (producto, precio, unidades)
  VALUES ( 'p6', 5, 10);
    

/*
  EJEMPLO 2
  Crear un disparador para la tabla ventas para que cuando inserte un registro me sume
  el total a la tabla productos (en el campo cantidad).
  corregido
*/
   DELIMITER $$
    CREATE OR REPLACE TRIGGER ventasAI
      AFTER INSERT
        ON ventas
        FOR EACH ROW
      BEGIN
        UPDATE productos SET cantidad=cantidad+NEW.total
          WHERE producto=NEW.producto;
    END $$
   DELIMITER ;
    INSERT INTO ventas (producto, precio, unidades)
   VALUES ('p2',10,24);

 
/*
  EJEMPLO 3
  Crear un disparador para la tabla ventas para que cuando actualices un registro nuevo
  te calcule el total automáticamente.   
  corregido   
*/

   DELIMITER %%
    CREATE OR REPLACE TRIGGER ventasBU
      BEFORE UPDATE
        ON ventas
        FOR EACH ROW
      BEGIN
        SET NEW.total=NEW.precio*NEW.unidades;
      END %%
   DELIMITER ;

UPDATE ventas
  SET unidades=67 WHERE id=16;
 
  SELECT * FROM ventas;
  SELECT * FROM productos;
/*
  EJEMPLO 4
  Crear un disparador para la tabla ventas para que cuando actualice un registro me
  sume el total a la tabla productos (en el campo cantidad).
  corregido
*/
   DELIMITER %%
    CREATE OR REPLACE TRIGGER ventasAU
      AFTER UPDATE 
        ON ventas
        FOR EACH ROW
      BEGIN
        UPDATE productos SET cantidad=cantidad+NEW.total 
          WHERE producto=new.producto;
        UPDATE productos SET cantidad=cantidad-old.total 
          WHERE producto=old.producto;
        /**
        UPDATE productos p SET p.cantidad=p.cantidad+(NEW.total-OLD.total) WHERE p.producto=NEW.producto;  
        **/

      END %%
   DELIMITER ;

  UPDATE ventas set unidades=8 WHERE ID=16;
  UPDATE ventas set unidades=24 WHERE ID=14;

/*
  EJEMPLO 5
  Crear un disparador para la tabla productos que si cambia el código del producto te
  sume todos los totales de ese producto de la tabla ventas
  corregido
*/

   DELIMITER |
    CREATE OR REPLACE TRIGGER productosBU
      BEFORE UPDATE 
        ON productos
        FOR EACH ROW
      BEGIN
        
          SET new.cantidad=(SELECT SUM(total) FROM ventas WHERE producto=new.producto);
        
      END |
   DELIMITER ;

SELECT * FROM ventas;
SELECT * FROM productos;

UPDATE productos set producto='p2' WHERE producto='p5';


  
/*
  EJEMPLO 6
  Crear un disparador para la tabla productos que si eliminas un producto te elimine
  todos los productos del mismo código en la tabla ventas
  corregido
  Tengo que realizar este disparador mediante claves ajenas (cascade) porque si no
  entra en conflicto con productosBU
*/

  DELIMITER |
   CREATE OR REPLACE TRIGGER productosAD
     AFTER DELETE
       ON productos
       FOR EACH ROW
     BEGIN  
       DELETE FROM ventas WHERE producto=old.producto;   
     END |
  DELIMITER ;

DELETE FROM productos WHERE producto='p8';

SELECT * FROM ventas;
SELECT * FROM productos;

/*
  EJEMPLO 7
  Crear un disparador para la tabla ventas que si eliminas un registro te reste el total del
  campo cantidad de la tabla productos (en el campo cantidad).
  corregido
*/
   DELIMITER |
    CREATE OR REPLACE TRIGGER ventasAD
      AFTER DELETE
        ON ventas
        FOR EACH ROW
      BEGIN
  
         UPDATE productos 
          SET cantidad=cantidad-old.total WHERE producto=old.producto;
  
      END |
   DELIMITER ;


/*
  EJEMPLO 8
  Modificar el disparador 3 para que modifique la tabla productos actualizando el valor
  del campo cantidad en funcion del total.
*/

   DELIMITER |
    CREATE OR REPLACE TRIGGER ventasBU
      BEFORE UPDATE
        ON ventas
        FOR EACH ROW
      BEGIN
  
        SET NEW.total=NEW.unidades*NEW.precio;

        UPDATE productos SET cantidad=cantidad+NEW.total WHERE producto=new.producto;
        UPDATE productos SET cantidad=cantidad-NEW.total WHERE producto=old.producto;
        /*UPDATE productos SET cantidad=(SELECT SUM(total) FROM ventas WHERE producto=new.producto);
        UPDATE productos SET cantidad=(SELECT SUM(total) FROM ventas WHERE producto=old.producto);*/

      /**
        -- Forma solucion cuaderno
        UPDATE productos p 
          SET p.cantidad=p.cantidad+(NEW.total-OLD.total)
          WHERE p.producto=NEW.producto;
      **/
      END |
   DELIMITER ;