﻿DROP DATABASE IF EXISTS ejemplo3programacion;
CREATE DATABASE ejemplo3programacion;
USE ejemplo3programacion;

/*
  Función área triangulo
*/

DELIMITER &&
CREATE OR REPLACE FUNCTION areaTriangulo(base int, altura int)
  RETURNS float
BEGIN
  DECLARE area float DEFAULT 0;

  SET area=base*altura/2;
  RETURN area;
END &&
DELIMITER ;


/*
  Función perímetro triangulo
*/


DELIMITER &&
CREATE OR REPLACE FUNCTION perimetroTriangulo(base int, lado2 int, lado3 int)
  RETURNS int
BEGIN
  DECLARE perimetro int DEFAULT 0;

  SET perimetro=base+lado2+lado3;
 
  RETURN perimetro;
END &&
DELIMITER ;

SELECT perimetroTriangulo(2,3,5);

/*
  Procedimiento almacenado triángulos
*/

DELIMITER $$
CREATE OR REPLACE PROCEDURE actualizarTriangulos(Id1 int, Id2 int)
  BEGIN
      UPDATE triangulos
        SET area=areaTriangulo(base,altura),
            perimetro=perimetroTriangulo(base, lado2, lado3)
      WHERE 
        id BETWEEN Id1 AND Id2;
  END $$
DELIMITER ;

/*
  Función área cuadrado
*/

DELIMITER &&
CREATE OR REPLACE FUNCTION areaCuadrado(lado int)
  RETURNS int
BEGIN
    DECLARE area int DEFAULT 0;
    
    SET area=POW(lado,2);
  RETURN area;
END &&
DELIMITER ;

SELECT areaCuadrado(3);

/*
  Función perímetro cuadrado
*/

DELIMITER &&
CREATE OR REPLACE FUNCTION perimetroCuadrado(lado int)
  RETURNS int
BEGIN
  DECLARE perimetro int DEFAULT 0;

  SET perimetro=lado*4;
 
  RETURN perimetro;
END &&
DELIMITER ;

SELECT perimetroCuadrado(4);

/*
  Procedimiento almacenado cuadrados
*/


DELIMITER $$
CREATE OR REPLACE PROCEDURE actualizarCuadrados(Id1 int, Id2 int)
  BEGIN
    UPDATE cuadrados 
      SET area=areaCuadrado(lado),
          perimetro=perimetroCuadrado(lado)
    WHERE
      id BETWEEN Id1 AND Id2;
  END $$
DELIMITER ;

/*
  Función área rectángulo
*/


DELIMITER &&
CREATE OR REPLACE FUNCTION areaRectangulo(lado1 int, lado2 int)
  RETURNS int
BEGIN
  DECLARE area int DEFAULT 0;

  SET area=lado1*lado2;
 
  RETURN area;
END &&
DELIMITER ; 

/*
  Función perímetro rectángulo
*/

DELIMITER &&
CREATE OR REPLACE FUNCTION perimetroRectangulo(lado1 int, lado2 int)
  RETURNS int
BEGIN
  DECLARE perimetro int DEFAULT 0;

  SET perimetro=2*lado1+2*lado2;

  RETURN perimetro;
END &&
DELIMITER ;

/*
  Procedimiento almacenado rectángulo
*/


DELIMITER $$
CREATE OR REPLACE PROCEDURE actualizarRectangulo(Id1 int,Id2 int)
  BEGIN
    UPDATE rectangulo
      SET area=areaRectangulo(lado1, lado2),
          perimetro=perimetroRectangulo(lado1, lado2)
    WHERE
      id BETWEEN Id1 AND Id2;  

  END $$
DELIMITER ;


/*
  Función área círculo
*/


DELIMITER &&
CREATE OR REPLACE FUNCTION areaCirculo(radio int)
  RETURNS float
BEGIN
  DECLARE area float DEFAULT 0;

  SET area=PI()*POW(radio,2);
  
  RETURN area;
END &&
DELIMITER ; 


SELECT areaCirculo(2);

/*
  Función perímetro círculo
*/


DELIMITER &&
CREATE OR REPLACE FUNCTION perimetroCirculo(radio int)
  RETURNS float
BEGIN
  DECLARE perimetro float DEFAULT 0;

  SET perimetro=2*PI()*radio;
 
  RETURN perimetro;
END &&
DELIMITER ; 

/*
  Procedimiento almacenado círculos
*/


DELIMITER $$
CREATE OR REPLACE PROCEDURE actualizarCirculos(Id1 int, Id2 int, tipoC varchar(5))
  BEGIN
     IF (tipoC IS NULL) THEN
     	UPDATE circulo
      SET area=areaCirculo(radio),
          perimetro=perimetroCirculo(radio)
      WHERE id BETWEEN Id1 AND Id2;
     ELSE
       UPDATE circulo
        SET area=areaCirculo(radio),
            perimetro=perimetroCirculo(radio)
        WHERE id BETWEEN Id1 AND Id2
        AND 
          tipo=tipoC;
     END IF;
     
  END $$
DELIMITER ;

/*
  Función media
*/


DELIMITER &&
CREATE OR REPLACE FUNCTION media(n1 int, n2 int, n3 int, n4 int)
  RETURNS float
BEGIN
  DECLARE notaMedia float DEFAULT 0;

  SET notaMedia=(n1+n2+n3+n4)/4;
 
  RETURN notaMedia;
END &&
DELIMITER ;


/*
  Función mínimo
*/


DELIMITER &&
CREATE OR REPLACE FUNCTION minimo(n1 int, n2 int, n3 int, n4 int)
  RETURNS int
BEGIN
  DECLARE minNota int DEFAULT 0;

  SET minNota=LEAST(n1, n2, n3, n4);
 
  RETURN minNota;
END &&
DELIMITER ;


/*
  Función máximo
*/


DELIMITER &&
CREATE OR REPLACE FUNCTION maximo(n1 int, n2 int, n3 int, n4 int)
  RETURNS int
BEGIN
  DECLARE maxNota int DEFAULT 0;

  SET maxNota=GREATEST(n1, n2, n3, n4);
 
  RETURN maxNota;
END &&
DELIMITER ;


/*
  Función moda
*/


DELIMITER &&
CREATE OR REPLACE FUNCTION moda(n1 int, n2 int, n3 int, n4 int)
  RETURNS int
BEGIN
  DECLARE moda int DEFAULT 0;
 
  /* Dentro de una función no te deja crear una tabla, tiene que ser tabla temporary
     Pero dentro de un procedimiento si puedes crear tabla
  */
  CREATE OR REPLACE TEMPORARY TABLE numeros(
    id int AUTO_INCREMENT PRIMARY KEY,
    dato int
    );

  INSERT INTO numeros (dato) VALUES (n1),(n2),(n3),(n4);
 
  /*SET moda=(SELECT dato FROM numeros GROUP BY dato HAVING COUNT(*)=(SELECT MAX(n) FROM (SELECT dato, COUNT(*) n FROM numeros GROUP BY dato) c1));*/
    SELECT dato INTO moda FROM numeros GROUP BY dato ORDER BY COUNT(*) DESC limit 1;


  RETURN moda;
END &&
DELIMITER ;

SELECT moda(2, 3, 5, 5);
DROP TEMPORARY TABLE numeros;


/*
  Procedimiento almacenado alumnos
*/

  DELIMITER $$
  CREATE OR REPLACE PROCEDURE actualizarAlumnos(Id1 int, Id2 int)
    BEGIN
      UPDATE alumnos
        SET media=media(nota1, nota2, nota3, nota4),
            max=maximo(nota1, nota2, nota3, nota4),
            min=minimo(nota1, nota2, nota3, nota4),
            moda=moda(nota1, nota2, nota3, nota4)
      WHERE id BETWEEN Id1 AND Id2;        

    -- cuando el grupo es 1
      UPDATE grupos
        SET media= (SELECT AVG(media) FROM alumnos WHERE id BETWEEN Id1 AND Id2 AND grupo=1) WHERE id=1;
    -- cuando el grupo es 2
      UPDATE grupos
        SET media=(SELECT AVG(media) FROM alumnos WHERE id BETWEEN Id1 AND Id2 AND grupo=2) WHERE id=2;
      END $$
  DELIMITER ;
  
CALL actualizarAlumnos(2, 8);

/*
  Procedimiento almacenado conversión
*/

  DELIMITER $$
  CREATE OR REPLACE PROCEDURE actualizarConversion(id1 int)
    BEGIN


    UPDATE conversion
      SET m=cm/100,km=cm/100000,pulgadas=cm/2.54
      WHERE cm IS NOT NULL AND id>id1;
    
    
    UPDATE conversion
      SET cm=m*100,km=m/1000,pulgadas=m*39.37
      WHERE m IS NOT NULL AND id>id1;

    UPDATE conversion
      SET cm=km*100000,m=km*1000,pulgadas=km*39370.079
      WHERE km IS NOT NULL AND id>id1;

     UPDATE conversion
      SET m=pulgadas/39.37,cm=pulgadas*2.54,km=pulgadas/39370.079
      WHERE pulgadas IS NOT NULL AND id>id1;

    END $$
  DELIMITER ;
  
CALL actualizarConversion(1);

SELECT * FROM conversion;